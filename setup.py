from setuptools import setup, find_packages

VERSION = "0.1.0"

DEPENDENCIES = [
    "numpy",
    "pandas",
    "h5py"
]

setup(
    name="matrixdb",
    version=VERSION,
    description="Local and remote indexed 2D matrix storage and querying.",
    author="Cory Giles",
    author_email="mail@corygil.es",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
        "Natural Language :: English",
        "Operating System :: POSIX",
        "Programming Language :: Python :: 3.6",
        "Topic :: Scientific/Engineering :: Bio-Informatics"
    ],
    license="AGPLv3+",

    include_package_data=True,
    packages=find_packages(),
    
    install_requires=DEPENDENCIES,
    setup_requires=["pytest-runner"],
    tests_require=["pytest"],
)
