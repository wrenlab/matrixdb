import sys

import click

from .core import EmbeddedMatrixDB

@click.group()
def cli():
    """
    MatrixDB command-line interface.
    """
    pass

@cli.command()
@click.argument("db_path")
@click.argument("matrix_key")
def load(db_path, matrix_key):
    """
    Load a matrix into a MatrixDB.
    """
    db = EmbeddedMatrixDB(db_path, read_only=False)
    db.put(matrix_key, sys.stdin)

@cli.command()
@click.argument("db_path")
@click.argument("matrix_key")
def dump(db_path, matrix_key):
    """
    Export a matrix from MatrixDB.
    """
    db = EmbeddedMatrixDB(db_path, read_only=False)
    X = db[matrix_key]
    X.dump(sys.stdout)

@cli.command()
@click.argument("db_path")
def ls(db_path):
    db = EmbeddedMatrixDB(db_path, read_only=False)
    for k in db:
        print(k, db[k].shape)

if __name__ == "__main__":
    cli()
    #db = MatrixDB("test.h5", read_only=False)
    #X = db.put("test", "test.tsv")
    #print(X.row("R15"))
    #print(X.column("C22"))
