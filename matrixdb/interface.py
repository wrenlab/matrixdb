from abc import ABC, abstractmethod

import pandas as pd

class Selection(object):
    def __init__(self, rix, cix, X):
        self._rix = pd.Index(rix)
        self._cix = pd.Index(cix)
        self._X = X

    def __repr__(self):
        return "<Selection with shape: ({},{})>".format(*self.shape)

    @property
    def row_index(self):
        return self._rix

    @property
    def column_index(self):
        return self._cix

    def row(self, key):
        o = pd.Series(self._X._row(key), index=self._X.column_index)
        return o.loc[self._cix]

    def rows(self, ix):
        assert len(set(ix) - set(self._rix)) == 0
        ix = list(ix)
        return Selection(ix, self._cix, self._X)

    def column(self, key):
        o = pd.Series(self._X._column(key), index=self._X.row_index)
        return o.loc[self._rix]

    def columns(self, ix):
        assert len(set(ix) - set(self._cix)) == 0
        return Selection(self._rix, ix, self._X)

    @property
    def shape(self):
        return (len(self._rix), len(self._cix))

    def to_array(self):
        return np.array(self.to_frame())

    def to_frame(self):
        rix = list(self._rix)
        cix = list(self._cix)
        if len(rix) < len(cix):
            o = pd.DataFrame(self._X._rows(rix), index=rix, columns=self._X.column_index)
            return o.loc[:,cix]
        else:
            o = pd.DataFrame(self._X._columns(cix), index=self._X.row_index, columns=cix)
            return o.loc[rix,:]

class IMatrix(ABC):
    """
    Interface for a generic matrix (or subset thereof) with labeled rows
    and columns.

    The primary purpose is to allow lazy subsetting for situations
    where, for example, the actual underlying data may not be 
    always in RAM in its entirety.
    """
    @property
    @abstractmethod
    def row_index(self):
        pass

    @property
    @abstractmethod
    def column_index(self):
        pass

    @abstractmethod
    def _row(self, key):
        pass

    @abstractmethod
    def _column(self, key):
        pass

    @abstractmethod
    def _rows(self, ix):
        pass

    @abstractmethod
    def _columns(self, ix):
        pass

    @abstractmethod
    def to_array(self):
        pass

    #####################
    # Implemented methods
    #####################

    def row(self, key):
        return pd.Series(self._row(key), index=self.column_index)

    def column(self, key):
        return pd.Series(self._column(key), index=self.row_index)

    def rows(self, ix):
        return Selection(ix, self.column_index, self)
        #o = self._rows(ix)
        #return pd.DataFrame(o, index=ix, columns=self.column_index)

    def columns(self, ix):
        return Selection(self.row_index, ix, self)
        #o = self._columns(ix)
        #return pd.DataFrame(o, index=self.row_index, columns=ix)

    def to_frame(self):
        """
        Read all the contents of this MatrixDB into a
        :class:`pandas.DataFrame`.

        NOTE: May consume lots of memory.
        """
        return pd.DataFrame(self.to_array(), 
                index=self.row_index, columns=self.column_index)

    @property
    def shape(self):
        return (len(self.row_index), len(self.column_index))
