Usage
=====

EmbeddedMatrixDB
----------------

"Embedded" usage allows you to access a `MatrixDB` which is stored in a local
file. In Python, you can 

Implementation
==============

`MatrixDB` has several API layers. From the lowest to highest level:

1. C++ implementation
2. C API
3. Python bridge to C API using :mod:`ctypes`
4. A :mod:`zmq`-based client-server API

The source and headers for #1 and #2 are found in `libwrenlab`.

The Python-C bridge, encapsulated in :class:`matrixdb.core.EmbeddedMatrixDB`,
is found in :mod:`matrixdb.core`.

The client/server API is found in :mod:`matrixdb.server` and
:mod:`matrixdb.client`.

File format
-----------

A MatrixDB is actually a SQLite3 database. The matrix data is stored in two
tables, `data` and `data_t`, each which have two columns, `key` and `data`,
along with the primary key which maintains ordering. The key corresponds to a
string storing the row/column label, and `data` stores a blob containing the
raw double array after zlib compression.

Thus, the matrix data is stored twice, in row-major and column-major formats,
for quick access of data on either axis. For each row, the uncompressed data
size is `sizeof(double) * n_columns`, and for each column, `sizeof(double) *
n_rows`. Each MatrixDB can store exactly one matrix.
