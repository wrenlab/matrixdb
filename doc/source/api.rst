===
API
===

.. automodule:: matrixdb.core
    :members:
    :undoc-members:

.. automodule:: matrixdb.client
    :members:
    :undoc-members:

.. automodule:: matrixdb.server
    :members:
    :undoc-members:
